Auckland’s best DJs for hire. Our Auckland DJs are highly regarded entertainment professionals who have honed their skills over a lifetime. Our DJs will read the crowd and move the room like no one else can. Our DJs are committed to ensuring your night is a resounding success.


Address: L1/139/141 Victoria Street West, Auckland CBD, Auckland 1010, New Zealand


Phone: +64 21 889 551


Website: https://www.findadj.co.nz/
